function(doc) {
	"use strict";

	if (doc.auteur)
		emit(doc.auteur, {titre:doc.titre||"[Sans titre]", _id:doc._id, date:doc.date, lieu:doc.lieu});
	}
