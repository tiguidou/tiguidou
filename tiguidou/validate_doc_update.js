function(newDoc, oldDoc, userCtx, secObj) {
	"use strict";

	if (-1 === userCtx.roles.indexOf("tiguidou")) throw({unauthorized: 'Not logged in?'});
	if (oldDoc && oldDoc.auteur !== newDoc.auteur) throw({unauthorized: 'Pas votre contenu?'});
}
