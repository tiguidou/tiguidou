function list(head, req) {
	"use strict";

	var doc, html, row, templates = require('views/lib/lodash.tpl').templates;

	start({ "headers": { "Content-Type": "text/html" }});

	html = "<ul>";

	while ((row = getRow())) {
		html = html + "<li><a href='/editer/" + row.id + "'>" + row.value.titre + " (" + row.value.date + " " + row.value.lieu + ")</a></li>";
	}
	html = html + "</ul>";

	doc = { nav: templates.nav({name:(-1 !== req.userCtx.roles.indexOf("tiguidou")) && req.userCtx.name ? req.userCtx.name : false}), main: html, title: "Exemples" };

	send(templates["par-auteur"](doc));
}
