function update(doc, req) {
    "use strict";
    
    var resp = {
		headers: {location: "/editer/" + req.id}, code: 302, body: "redirection..."
	}, _ = require('views/lib/lodash.min');

	if (!doc) { doc = { _id: req.id }; }
	doc.texte = req.form.texte.trim().replace("\r", "", "g");
	doc.titre = req.form.titre.trim();
	doc.auteur = req.userCtx.name;
	doc.date = req.form.date.trim();
	doc.lieu = req.form.lieu.trim();
	doc.theme = req.form.theme.trim();
	doc.transition = req.form.transition.trim();
	doc.separator = req.form.separator;
	doc.separator_regex = req.form.separator_regex;
	doc.vertical = req.form.vertical;
	
    return [(req.form.titre&&req.form.texte&&(doc.date||doc.lieu))?doc:null, resp];
}
