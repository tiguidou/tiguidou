function(doc, req) {
    "use strict";

	return '<html><head><title>yup</title><script src="/reveal/plugin/markdown/marked.js"></script></head><body><script>document.write(marked("' + doc.texte.replace("\n", "\\n", "g") + '"));</script></body></html>';
}
