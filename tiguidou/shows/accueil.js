function(doc, req) {
    "use strict";

    var marked = require('views/lib/marked'),
		templates = require('views/lib/lodash.tpl').templates;

	doc = { nav: templates.nav({name:(-1 !== req.userCtx.roles.indexOf("tiguidou")) && req.userCtx.name ? req.userCtx.name : false}) };

	if (req.query.page && ("colophon" === req.query.page)) {
		doc.main = marked(templates.colophon());
		doc.title = "Colophon";
	} else {
		doc.title = "Tiguidou";
	}
	
	return templates.accueil(doc);	
}
