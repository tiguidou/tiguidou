function(doc, req) {
    "use strict";

    var no_doc = !doc, templates = require('views/lib/lodash.tpl').templates;

	if (no_doc) doc = {_id: req.id?req.id:req.uuid };
	
	doc.name = (-1 !== req.userCtx.roles.indexOf("tiguidou")) && req.userCtx.name ? req.userCtx.name : false;


	if (no_doc)
		doc.nav = templates.nav({name:doc.name});
	else
		doc.nav = templates.nav({name:doc.name, submenu: templates.submenu({_id:doc._id})});

	return templates.form(doc);	
}
