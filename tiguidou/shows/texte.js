function(doc, req) {
    "use strict";

    var templates = require('views/lib/lodash.tpl').templates,
    _ = require('views/lib/lodash.min');
	
	return templates.example({o: {
		id: doc._id,
		titre: doc.titre||"[Sans titre]",
		theme: doc.theme||"default",
		transition: doc.transition||"default",
		separator_regex: (doc.separator_regex||"^\n\n\n").replace("\n", "\\n", "g"),
		separator: (doc.separator||"\n\n\n").replace("\n", "\\n", "g"),
		vertical: (doc.vertical||"^\n\n").replace("\n", "\\n", "g")
	}});
}