function(doc, req) {
    "use strict";

    var parts = [], templates = require('views/lib/lodash.tpl').templates;//,

	if (doc.titre || doc.auteur || doc.date) parts.push(templates.titre(doc));
	if (doc.texte) parts.push(doc.texte);
	parts.push(templates.colophon());
	
	return {
		body : parts.join((doc.separator||"\n\n\n").replace("\\n", "\n", "g")),
		headers : { "Content-Type" : "text/plain; charset=utf-8" }
	};
}
